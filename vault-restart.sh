#!/bin/bash -l

#------------------------------------------------------------------------------------------------
#title:        vault-restart.sh
#description:  This script is responsible for restarting and unsealing vault.
#about:        Unseals vault on exceptional events (e.g. system re-start, certbot renew)
#date:         2020-08-03 10:40
#version:      1.0
#usage:        ./vault-restart.sh
#Requires:     1) /etc/vault/init.file
#              2) /usr/local/bin/vault
#-----------------------------------------------------------------------------------------------

function main(){
     systemctl restart vault.service && egrep -m3 '^Unseal Key' /etc/vault/init.file | cut -f2- -d: | tr -d ' ' | while read key; do   sleep 3;/usr/local/bin/vault operator unseal ${key}; done
}

main
