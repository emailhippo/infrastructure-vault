#!/bin/bash -l

#------------------------------------------------------------------------------------------------
#title:        unseal-vault.sh
#description:  This script is responsible for unsealing vault after a successful certbot renewal.
#about:        Invokes ---deploy-hook after successful certbot renewal.
#date:         2020-08-03 10:40
#version:      1.0
#usage:        ./unseal-vault.sh
#Requires:     1) Must be placed in /etc/letsencrypt/renewal-hooks/deploy
#              2) Requires vault-restart.sh
#-----------------------------------------------------------------------------------------------

## Change to script directory
cd "${0%/*}"

set -ex

## main function
function main(){
        /root/helpers/infrastructure-vault/vault-restart.sh >> /root/logs/vault-restart.log 2>&1
}

## execute
main
